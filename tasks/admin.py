from django.contrib import admin
from tasks.models import Task


class Taskadmin(admin.ModelAdmin):
    pass


admin.site.register(Task, Taskadmin)
